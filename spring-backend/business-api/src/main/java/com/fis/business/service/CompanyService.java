package com.fis.business.service;

import com.fis.business.entity.Company;
import com.fis.business.form.filter.CompanyFilterForm;
import com.fis.business.form.inesrt.CompanyCreateForm;
import com.fis.business.form.update.CompanyUpdateForm;
import com.fis.fw.common.generics.GenericService;

import java.util.List;

public interface CompanyService extends GenericService<Company,Integer> {

    public List<Company> getAllCompany();

    public List<Company> findCompanyByFilter(CompanyFilterForm form);

    public void createCompany(CompanyCreateForm form);

    public Company getCompanyById(Integer id);

    public boolean existsCompanyById(Integer id);

    public void updateCompany(Integer id, CompanyUpdateForm form);

    public void deleteCompanyById(Integer id);

    public void deleteAllCompany(List<Integer> ids);

}
