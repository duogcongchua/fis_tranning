package com.fis.business.repo;

import com.fis.business.dao.TimeKeepingDao;
import com.fis.business.entity.TimeKeeping;
import io.lettuce.core.dynamic.annotation.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface TimeKeepingRepo extends JpaRepository<TimeKeeping,Integer>, JpaSpecificationExecutor<TimeKeeping>, TimeKeepingDao {

    public TimeKeeping getTimeKeepingById(Integer id);

    public List<TimeKeeping> getAllByCompanyId(Integer id);

    @Modifying
    @Query(value = "DELETE FROM TimeKeeping tk WHERE tk.id IN (:ids)")
    public void deleteAllTimeKeeping(@Param(value = "ids") List<Integer> ids);
}
