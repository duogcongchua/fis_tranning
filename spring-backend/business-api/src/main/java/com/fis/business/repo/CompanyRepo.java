package com.fis.business.repo;

import com.fis.business.dao.CompanyDao;
import com.fis.business.entity.Company;
import io.lettuce.core.dynamic.annotation.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CompanyRepo extends JpaRepository<Company,Integer>, JpaSpecificationExecutor<Company>, CompanyDao {

    public Company getCompanyById(Integer id);

    public boolean existsCompanyById(Integer id);

    @Modifying
    @Query(value = "DELETE FROM Company c WHERE c.id IN (:ids)")
    public void deleteAllCompany(@Param(value = "ids") List<Integer> ids);
}
