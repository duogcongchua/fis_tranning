package com.fis.business.service.impl;

import com.fis.business.entity.Company;
import com.fis.business.entity.TimeKeeping;
import com.fis.business.form.filter.TimeKeepingFilterFrom;
import com.fis.business.form.inesrt.InsertFormTimeKeeping;
import com.fis.business.form.update.TimeKeepingUpdateForm;
import com.fis.business.repo.CompanyRepo;
import com.fis.business.repo.TimeKeepingRepo;
import com.fis.business.service.TimeKeepingService;
import com.fis.business.specification.TimeKeepingSpecification;
import com.fis.fw.common.generics.impl.GenericServiceImpl;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityExistsException;
import java.sql.Time;
import java.util.List;

@Service
@Transactional
public class TimeKeepingServiceImpl extends GenericServiceImpl<TimeKeeping,Integer> implements TimeKeepingService {

    @Autowired
    private TimeKeepingRepo timeKeepingRepo;

    @Autowired
    private CompanyRepo companyRepo;

    @Autowired
    private ModelMapper modelMapper;


    @Override
    public TimeKeeping getTimeKeepingById(Integer id) {
        return timeKeepingRepo.getTimeKeepingById(id);
    }

    @Override
    public void createTimeKeeping(InsertFormTimeKeeping form) {
        Company company = modelMapper.map(form.getCompanyCreateForm(),Company.class);
        companyRepo.save(company);
        List<TimeKeeping> timeKeepings = modelMapper.map(form.getTimeKeepingCreateForms(),new TypeToken<List<TimeKeeping>>(){}.getType());
        for (TimeKeeping item : timeKeepings){
            item.setCompanyId(company.getId());
            timeKeepingRepo.save(item);

        }
    }

    @Override
    public List<TimeKeeping> getTimeKeepingByCompanyId(Integer id) {
        return timeKeepingRepo.getAllByCompanyId(id);
    }

    @Override
    public List<TimeKeeping> getAllTimeKeepingByFilter(TimeKeepingFilterFrom from) {
        Specification<TimeKeeping> where = TimeKeepingSpecification.buildWhere(from);
        return timeKeepingRepo.findAll(where);
    }

    @Override
    public void updateTimeKeeping(Integer id, TimeKeepingUpdateForm form) {
        TimeKeeping timeKeeping = timeKeepingRepo.getTimeKeepingById(id);
        timeKeeping.setDayInWeek(form.getDayInWeek());
        timeKeeping.setCheckIn(form.getCheckIn());
        timeKeeping.setCheckOut(form.getCheckOut());
        timeKeeping.setStartShiftOff(form.getStartShiftOff());
        timeKeeping.setEndShiftOff(form.getEndShiftOff());

        timeKeepingRepo.save(timeKeeping);
    }

    @Override
    public void deleteTimeKeepingById(Integer id) {
        TimeKeeping timeKeeping = timeKeepingRepo.getTimeKeepingById(id);
        if (timeKeeping != null){
            timeKeepingRepo.delete(timeKeeping);
        }else {
            throw new EntityExistsException("Time keeping dose n't exits");
        }
    }

    @Override
    public void deleteAllTimeKeeping(List<Integer> ids) {
        timeKeepingRepo.deleteAllTimeKeeping(ids);
    }
}
