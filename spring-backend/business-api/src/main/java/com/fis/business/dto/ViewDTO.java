package com.fis.business.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ViewDTO {

    @JsonProperty("COM_ID")
    private Integer id;

    @JsonProperty("COM_NAME")
    private String name;

    private List<KeepingDTO> keepingDTOList;


    @Data
    public static class KeepingDTO{

        @JsonProperty("Day in week")
        private String dayInWeek;

        @JsonProperty("Check in")
        private String checkIn;

        @JsonProperty("Check out")
        private String checkOut;
    }

}
