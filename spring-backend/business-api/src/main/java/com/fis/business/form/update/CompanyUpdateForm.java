package com.fis.business.form.update;

import lombok.Data;

@Data
public class CompanyUpdateForm {

    private Integer group;

    private Integer typeShift;

    private String status;

    private String startShift;

    private String endShift;
}
