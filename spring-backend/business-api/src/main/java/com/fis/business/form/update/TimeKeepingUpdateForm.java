package com.fis.business.form.update;

import lombok.Data;

@Data
public class TimeKeepingUpdateForm {

    private String dayInWeek;

    private String checkIn;

    private String checkOut;

    private String startShiftOff;

    private String endShiftOff;
}
