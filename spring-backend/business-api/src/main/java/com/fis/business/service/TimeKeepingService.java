package com.fis.business.service;

import com.fis.business.entity.TimeKeeping;
import com.fis.business.form.filter.TimeKeepingFilterFrom;
import com.fis.business.form.inesrt.InsertFormTimeKeeping;
import com.fis.business.form.update.TimeKeepingUpdateForm;
import com.fis.fw.common.generics.GenericService;
import io.swagger.models.auth.In;

import java.sql.Time;
import java.util.List;

public interface TimeKeepingService extends GenericService<TimeKeeping,Integer> {

    public TimeKeeping getTimeKeepingById(Integer id);

    public void createTimeKeeping(InsertFormTimeKeeping insertFormTimeKeeping);

    public List<TimeKeeping> getTimeKeepingByCompanyId(Integer integer);

    public List<TimeKeeping> getAllTimeKeepingByFilter(TimeKeepingFilterFrom from);

    public void updateTimeKeeping(Integer id, TimeKeepingUpdateForm form);

    public void deleteTimeKeepingById(Integer id);

    public void deleteAllTimeKeeping(List<Integer> ids);
}
