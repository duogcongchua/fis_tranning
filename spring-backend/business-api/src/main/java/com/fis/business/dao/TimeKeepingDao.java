package com.fis.business.dao;

import com.fis.business.entity.TimeKeeping;
import com.fis.fw.common.generics.GenericDao;

public interface TimeKeepingDao extends GenericDao<TimeKeeping,Integer> {
}
