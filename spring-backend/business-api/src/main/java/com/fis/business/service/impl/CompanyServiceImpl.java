package com.fis.business.service.impl;

import com.fis.business.dto.CompanyDTO;
import com.fis.business.dto.TimeKeepingDTO;
import com.fis.business.entity.Company;
import com.fis.business.entity.TimeKeeping;
import com.fis.business.form.filter.CompanyFilterForm;
import com.fis.business.form.inesrt.CompanyCreateForm;
import com.fis.business.form.update.CompanyUpdateForm;
import com.fis.business.repo.CompanyRepo;
import com.fis.business.repo.TimeKeepingRepo;
import com.fis.business.service.CompanyService;
import com.fis.business.specification.CompanySpecification;
import com.fis.fw.common.generics.impl.GenericServiceImpl;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityExistsException;
import java.util.List;

@Service
@Transactional
public class CompanyServiceImpl extends GenericServiceImpl<Company,Integer> implements CompanyService {

    @Autowired
    private CompanyRepo companyRepo;

    @Autowired
    private ModelMapper modelMapper;


    @Override
    public List<Company> getAllCompany() {
        return companyRepo.findAll();
    }

    @Override
    public List<Company> findCompanyByFilter(CompanyFilterForm form) {
        Specification<Company> where = CompanySpecification.buildWhere(form);
        return companyRepo.findAll(where);
    }

    @Override
    public void createCompany(CompanyCreateForm form) {
        Company company = modelMapper.map(form,Company.class);
        companyRepo.save(company);
    }

    @Override
    public Company getCompanyById(Integer id) {
        return companyRepo.getCompanyById(id);
    }

    @Override
    public boolean existsCompanyById(Integer id) {
        return companyRepo.existsCompanyById(id);
    }

    @Override
    public void updateCompany(Integer id, CompanyUpdateForm form) {
        Company company = companyRepo.getCompanyById(id);
        company.setGroup(form.getGroup());
        company.setTypeShift(form.getTypeShift());
        company.setStatus(form.getStatus());
        company.setStartShift(form.getStartShift());
        company.setEndShift(form.getEndShift());

        companyRepo.save(company);
    }

    @Override
    public void deleteCompanyById(Integer id) {
        Company company = companyRepo.getCompanyById(id);
        if (company != null){
            companyRepo.delete(company);
        }else {
            throw new EntityExistsException("Company doesn't exists");
        }
    }

    @Override
    public void deleteAllCompany(List<Integer> ids) {
        companyRepo.deleteAllCompany(ids);
    }
}
