package com.fis.business.dao.impl;

import com.fis.business.dao.TimeKeepingDao;
import com.fis.business.entity.TimeKeeping;
import com.fis.fw.common.generics.impl.GenericDaoImpl;
import com.fis.fw.common.generics.impl.GenericServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class TimeKeepingDaoImpl extends GenericDaoImpl<TimeKeeping,Integer> implements TimeKeepingDao {
}
