package com.fis.business.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.Column;

@Data
public class TimeKeepingDTO {

    @JsonProperty("Day in week")
    private String dayInWeek;

    @JsonProperty("Check in")
    private String checkIn;

    @JsonProperty("Check out")
    private String checkOut;

    @JsonProperty("Start shift off")
    private String startShiftOff;

    @JsonProperty("End shift off")
    private String endShiftOff;

    @JsonProperty("Company id")
    private Integer companyId;

    @JsonProperty("Day")
    private String day;
}
