package com.fis.business.form.filter;

import lombok.Data;

import java.util.Date;

@Data
public class CompanyFilterForm {

    private String code;

    private Integer publicStyle;

    private Date applyDay;
}
