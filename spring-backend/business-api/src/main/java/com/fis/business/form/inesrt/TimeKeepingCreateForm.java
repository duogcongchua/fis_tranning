package com.fis.business.form.inesrt;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class TimeKeepingCreateForm {

    @NotBlank(message = "Day in week can't null")
    private String dayInWeek;

    @NotBlank(message = "Check in can't null")
    private String checkIn;

    @NotBlank(message = "Check out can't null")
    private String checkOut;

    @NotBlank(message = "Start shift off can't null")
    private String startShiftOff;

    @NotBlank(message = "End shift off can't null")
    private String endShiftOff;

    @NotBlank(message = "Day can't null")
    private String day;

}
