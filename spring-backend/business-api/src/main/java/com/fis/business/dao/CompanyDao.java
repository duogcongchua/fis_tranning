package com.fis.business.dao;

import com.fis.business.entity.Company;
import com.fis.fw.common.generics.GenericDao;

public interface CompanyDao extends GenericDao<Company, Integer> {

}
