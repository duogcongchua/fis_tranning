package com.fis.business.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CompanyDTO {

    @JsonProperty("COM_ID")
    private Integer id;

    @JsonProperty("COM_NAME")
    private String name;

}
