package com.fis.business.specification;

import com.fis.business.entity.TimeKeeping;
import lombok.Data;
import lombok.NonNull;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@Data
public class TimeKeepingSpecificationCustom implements Specification<TimeKeeping> {

    @NonNull
    private String filter;

    @NonNull
    private Object value;

    @Override
    public Predicate toPredicate(Root<TimeKeeping> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {

        if (filter.equalsIgnoreCase("dayInWeek")){
            criteriaBuilder.like(root.get("dayInWeek"),"%" + value.toString() + "%");
        }

        if (filter.equalsIgnoreCase("checkIn")){
            criteriaBuilder.like(root.get("checkIn"),"%" + value.toString() + "%");
        }

        if (filter.equalsIgnoreCase("checkOut")){
            criteriaBuilder.like(root.get("checkOut"),"%" + value.toString() + "%");
        }
        return null;
    }
}
