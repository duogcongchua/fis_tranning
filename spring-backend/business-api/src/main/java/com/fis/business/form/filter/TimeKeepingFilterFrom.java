package com.fis.business.form.filter;

import lombok.Data;

@Data
public class TimeKeepingFilterFrom {

    private String dayInWeek;

    private String checkIn;

    private String checkOut;

}
