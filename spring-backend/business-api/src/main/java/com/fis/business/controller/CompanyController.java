package com.fis.business.controller;


import ch.qos.logback.core.encoder.EchoEncoder;
import com.fis.business.dto.CompanyDTO;
import com.fis.business.dto.MessagesResponse;
import com.fis.business.dto.ViewDTO;
import com.fis.business.entity.Company;
import com.fis.business.entity.TimeKeeping;
import com.fis.business.form.filter.CompanyFilterForm;
import com.fis.business.form.inesrt.CompanyCreateForm;
import com.fis.business.form.update.CompanyUpdateForm;
import com.fis.business.service.CompanyService;
import com.fis.business.service.TimeKeepingService;
import com.fis.fw.common.annotation.ApiRequestMapping;
import com.fis.fw.common.config.CoreConstants;
import com.fis.fw.common.utils.LogUtil;
import com.fis.fw.common.generics.controller.GenericController;
import io.swagger.models.auth.In;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/company-v1-api")
@RestController
@ApiRequestMapping(module = "Company")
@CrossOrigin("*")
public class CompanyController extends GenericController<Company,Integer>{

    @Autowired
    private CompanyService companyService;

    @Autowired
    private TimeKeepingService timeKeepingService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/allCompany")
    @PreAuthorize("@appAuthorizer.authorize(authentication,'" + CoreConstants.PRIVILEGE.VIEW + "',this)")
    public ResponseEntity<?> getAllCompany() {

        long startTime = System.currentTimeMillis();
        LogUtil.showLog(logger,LogUtil.LOG_BEGIN,"getAllCompany",startTime);
        List<CompanyDTO> companyDTOList = new ArrayList<>();
        MessagesResponse<List<CompanyDTO>> messagesResponse = new MessagesResponse<>();
        try{
            List<Company> list = companyService.getAllCompany();
            companyDTOList = modelMapper.map(list,new TypeToken<List<CompanyDTO>>(){}.getType());
            messagesResponse.setData(companyDTOList);
        }catch (Exception e){
            logger.error(e.getMessage(),e);
        }finally {
            LogUtil.showLog(logger,LogUtil.LOG_END,"getAllCompany",startTime);
        }
        return new ResponseEntity<>(messagesResponse, HttpStatus.OK);
    }

    @GetMapping("/getCompanyById")
    @PreAuthorize("@appAuthorizer.authorize(authentication,'" + CoreConstants.PRIVILEGE.VIEW + "',this)")
    public ResponseEntity<?> getCompanyById(@RequestParam(name = "id") Integer id){
        long timeStart = System.currentTimeMillis();
        LogUtil.showLog(logger,LogUtil.LOG_BEGIN,"getCompanyById",timeStart);
        ViewDTO  viewDTO = new ViewDTO();

        MessagesResponse<ViewDTO> messagesResponse = new MessagesResponse<>();

        try {
            Company company  = companyService.getCompanyById(id);
            List<TimeKeeping>  timeKeepingList= timeKeepingService.getTimeKeepingByCompanyId(id);
            List<ViewDTO.KeepingDTO> keepingDTO = modelMapper.map(timeKeepingList, new TypeToken<List<ViewDTO.KeepingDTO>>(){}.getType());

//            map bang tay
            viewDTO.setId(company.getId());
            viewDTO.setName(company.getName());
            viewDTO.setKeepingDTOList(keepingDTO);

            messagesResponse.setData(viewDTO);


        }catch (Exception e){
            logger.error(e.getMessage(),e);
            messagesResponse.error(e);
        }finally {
            LogUtil.showLog(logger,LogUtil.LOG_END,"getCompanyById",timeStart);
        }

        return new ResponseEntity<>(viewDTO,HttpStatus.OK);
    }

    @GetMapping("/getCompanyByFilter")
    @PreAuthorize("@appAuthorizer.authorize(authentication,'" + CoreConstants.PRIVILEGE.VIEW + "',this)")
    public ResponseEntity<?> getCompanyByFilter(CompanyFilterForm form){
        long startTime = System.currentTimeMillis();
        LogUtil.showLog(logger,LogUtil.LOG_BEGIN,"getCompanyByFilter",startTime);
        MessagesResponse<List<CompanyDTO>> messagesResponse = new MessagesResponse<>();
        List<CompanyDTO> companyDTOList = new ArrayList<>();

        try {
            List<Company> companyList = companyService.findCompanyByFilter(form);
            companyDTOList = modelMapper.map(companyList,new TypeToken<List<CompanyDTO>>(){}.getType());
            messagesResponse.setData(companyDTOList);
        }catch (Exception e){
            logger.error(e.getMessage(),e);
            messagesResponse.error(e);
        }finally {
            LogUtil.showLog(logger,LogUtil.LOG_END,"getCompanyByFilter",startTime);
        }

        return new ResponseEntity<>(messagesResponse,HttpStatus.OK);
    }
    @PostMapping
    @PreAuthorize("@appAuthorizer.authorize(authentication,'" + CoreConstants.PRIVILEGE.INSERT + "',this)")
    public ResponseEntity<?> createCompany(@RequestBody CompanyCreateForm form){
        long startTime = System.currentTimeMillis();
        LogUtil.showLog(logger,LogUtil.LOG_BEGIN,"createCompany",startTime);
        MessagesResponse messagesResponse = new MessagesResponse();

        try {
            companyService.createCompany(form);
            messagesResponse.setData(form);
            messagesResponse.setMessage(CoreConstants.MESSAGE.CREAT_SUCCESS);
        }catch (Exception e){
            logger.error(e.getMessage(),e);
            messagesResponse.error(e);
        }finally {
            LogUtil.showLog(logger,LogUtil.LOG_END,"createCompany",startTime);
        }

        return new ResponseEntity<>(messagesResponse,HttpStatus.OK);
    }

    @PostMapping("/updateCompany")
    @PreAuthorize("@appAuthorizer.authorize(authentication,'" + CoreConstants.PRIVILEGE.UPDATE + "',this)")
    public ResponseEntity<?> updateCompany(@RequestParam(name = "id") Integer id, @RequestBody CompanyUpdateForm form){
        long startTime = System.currentTimeMillis();
        LogUtil.showLog(logger,LogUtil.LOG_BEGIN,"updateCompany",startTime);
        MessagesResponse messagesResponse = new MessagesResponse();

        try{
            companyService.updateCompany(id,form);
            messagesResponse.setData(form);
            messagesResponse.setMessage(CoreConstants.MESSAGE.UPDATE_SUCCESS);
        }catch (Exception e){
            logger.error(e.getMessage(),e);
            messagesResponse.error(e);
        }finally {
            LogUtil.showLog(logger,LogUtil.LOG_END,"updateCompany",startTime);
        }

        return new ResponseEntity<>(messagesResponse,HttpStatus.OK);
    }

    @PostMapping("/deleteById")
    @PreAuthorize("@appAuthorizer.authorize(authentication,'" + CoreConstants.PRIVILEGE.DELETE + "',this)")
    public ResponseEntity<?> deleteCompanyById(@RequestParam(name = "id") Integer id){
        long timeStart = System.currentTimeMillis();
        LogUtil.showLog(logger,LogUtil.LOG_BEGIN,"deleteCompanyById",timeStart);
        MessagesResponse<CompanyDTO> messagesResponse = new MessagesResponse<>();
        CompanyDTO companyDTO = new CompanyDTO();

        try {
            companyDTO = modelMapper.map(companyService.existsCompanyById(id),CompanyDTO.class);
            messagesResponse.setData(companyDTO);
            companyService.deleteCompanyById(id);
            messagesResponse.setMessage(CoreConstants.MESSAGE.DELETE_SUCCESS);

        }catch (Exception e){
            logger.error(e.getMessage(),e);
            messagesResponse.error(e);
        }finally {
            LogUtil.showLog(logger,LogUtil.LOG_END,"deleteCompanyById",timeStart);
        }

        return new ResponseEntity<>(messagesResponse,HttpStatus.OK);
    }

    @PostMapping("/deleteAllCompany")
    @PreAuthorize("@appAuthorizer.authorize(authentication,'" + CoreConstants.PRIVILEGE.DELETE + "',this)")
    public ResponseEntity<?> deleteAllCompany(@RequestBody List<Integer> ids){
        long startTime = System.currentTimeMillis();
        LogUtil.showLog(logger,LogUtil.LOG_BEGIN,"deleteAllCompany",startTime);
        MessagesResponse<List<CompanyDTO>> messagesResponse = new MessagesResponse<>();
        List<CompanyDTO> companyDTOList = new ArrayList<>();
        List<Company> companyList = new ArrayList<>();
        try {

            for (Integer item : ids){
                Company company = companyService.getCompanyById(item);
                companyList.add(company);
            }

            companyDTOList = modelMapper.map(companyList,new TypeToken<List<CompanyDTO>>(){}.getType());
            messagesResponse.setData(companyDTOList);
            companyService.deleteAllCompany(ids);
            messagesResponse.setMessage(CoreConstants.MESSAGE.DELETE_SUCCESS);
        }catch (Exception e){
            logger.error(e.getMessage(),e);
            messagesResponse.error(e);
        }finally {
            LogUtil.showLog(logger,LogUtil.LOG_END,"deleteAllCompany",startTime);
        }

        return new ResponseEntity<>(messagesResponse,HttpStatus.OK);
    }
}
