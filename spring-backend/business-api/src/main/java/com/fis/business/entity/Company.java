package com.fis.business.entity;





import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "Company" ,schema = "FIS")
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
public class Company implements Serializable {

    @Id
    @Column(name = "COM_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMPANY_SEQ")
    @SequenceGenerator(schema = "FIS",sequenceName = "COMPANY_SEQ", name = "COMPANY_SEQ", allocationSize = 1)
    private Integer id;

    @Column(name = "COM_NAME",length = 225,nullable = false)
    private String name;

    @Column(name = "COM_CODE",length = 200,nullable = false)
    private String code;

    @Column(name = "COM_ADDRESS",length = 200,nullable = false)
    private String address;

    @Column(name = "S_NAMESHIFT",length = 200,nullable = false)
    private String nameShift;

    @Column(name = "S_GROUP",length = 200,nullable = false)
    private Integer group;

    @Column(name = "S_TS",length = 200,nullable = false)
    private Integer typeShift;

    @Column(name = "S_PS",nullable = false,length = 200)
    private Integer publicStyle;

    @Column(name = "S_VS",nullable = false,length = 200)
    private String valueShift;

    @Column(name = "S_S",length = 200,nullable = false)
    private String shift;

    @Column(name = "S_STATUS")
    private String status;

    @Column(name = "S_AD",nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date applyDay;

    @Column(name = "S_SS",nullable = false)
    private String startShift;

    @Column(name = "S_ES",nullable = false)
    private String endShift;
}
