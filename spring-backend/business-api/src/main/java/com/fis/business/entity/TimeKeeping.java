package com.fis.business.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "TimeKeeping",schema = "FIS")
@Getter
@Setter
@NoArgsConstructor
@DynamicInsert
@DynamicUpdate
public class TimeKeeping implements Serializable {

    @Id
    @Column(name = "TK_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "TIMEKEEPING_SQE")
    @SequenceGenerator(schema = "FIS",sequenceName = "TIMEKEEPING_SQE",name = "TIMEKEEPING_SQE", allocationSize = 1)
    private Integer id;

    @Column(name = "TK_DAY",nullable = false,length = 225)
    private String dayInWeek;

    @Column(name = "TK_CHECKIN",nullable = false,length = 225)
    private String checkIn;

    @Column(name = "TK_CHECKOUT",nullable = false,length = 225)
    private String checkOut;

    @Column(name = "TK_SSO",nullable = false,length = 225)
    private String startShiftOff;

    @Column(name = "TK_ESO",nullable = false,length = 225)
    private String endShiftOff;

    @Column(name = "TK_COMPANYID")
    private Integer companyId;

    @Column(name = "DAY",nullable = false,length = 225)
    private String day;
}
