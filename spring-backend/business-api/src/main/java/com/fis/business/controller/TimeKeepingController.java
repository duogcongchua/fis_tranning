package com.fis.business.controller;

import com.fis.business.dto.MessagesResponse;
import com.fis.business.dto.TimeKeepingDTO;
import com.fis.business.entity.TimeKeeping;
import com.fis.business.form.filter.TimeKeepingFilterFrom;
import com.fis.business.form.inesrt.InsertFormTimeKeeping;
import com.fis.business.form.update.TimeKeepingUpdateForm;
import com.fis.business.service.TimeKeepingService;
import com.fis.fw.common.annotation.ApiRequestMapping;
import com.fis.fw.common.config.CoreConstants;
import com.fis.fw.common.generics.controller.GenericController;
import com.fis.fw.common.utils.LogUtil;
import io.swagger.models.auth.In;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/timekeeping-v1-api")
@CrossOrigin("*")
@ApiRequestMapping(module = "TimeKeeping")
@Validated
public class TimeKeepingController extends GenericController<TimeKeeping,Integer> {

    @Autowired
    private TimeKeepingService timeKeepingService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    @PreAuthorize("@appAuthorizer.authorize(authentication,'" + CoreConstants.PRIVILEGE.VIEW + "',this)")
    public ResponseEntity<?> getTimeKeepingById(@RequestParam(name = "id") Integer id){
      long startTime = System.currentTimeMillis();
      LogUtil.showLog(logger,LogUtil.LOG_BEGIN,"getTimeKeepingById",startTime);
      TimeKeepingDTO timeKeepingDTO = new TimeKeepingDTO();
      MessagesResponse<TimeKeepingDTO> messagesResponse = new MessagesResponse<>();
      try {
          timeKeepingDTO = modelMapper.map(timeKeepingService.getTimeKeepingById(id),TimeKeepingDTO.class);
          messagesResponse.setData(timeKeepingDTO);
      }catch (Exception e) {
          logger.error(e.getMessage(),e);
          messagesResponse.error(e);
      }finally {
          LogUtil.showLog(logger,LogUtil.LOG_END,"getTimeKeepingById",startTime);
      }

      return new ResponseEntity<>(messagesResponse,HttpStatus.OK);
    }

    @PostMapping
    @PreAuthorize("@appAuthorizer.authorize(authentication,'" + CoreConstants.PRIVILEGE.INSERT + "',this)")
    public ResponseEntity<?> createTimeKeeping(@RequestBody @Valid InsertFormTimeKeeping insertFormTimeKeeping){

        long startTime = System.currentTimeMillis();
        LogUtil.showLog(logger,LogUtil.LOG_BEGIN,"createTimeKeeping",startTime);
        MessagesResponse messagesResponse = new MessagesResponse();

        try{
            timeKeepingService.createTimeKeeping(insertFormTimeKeeping);
            messagesResponse.setMessage(CoreConstants.MESSAGE.CREAT_SUCCESS);
            messagesResponse.setData(insertFormTimeKeeping);
        }catch (Exception e){
            logger.error(e.getMessage(),e);
            messagesResponse.error(e);
            System.out.println(messagesResponse);
        }finally {
            LogUtil.showLog(logger,LogUtil.LOG_END,"createTimeKeeping",startTime);
        }

        return new ResponseEntity<>(messagesResponse, HttpStatus.OK);
    }

    @GetMapping("/getTimeKeepingByFilter")
    @PreAuthorize("@appAuthorizer.authorize(authentication,'" + CoreConstants.PRIVILEGE.VIEW + "',this)")
    public ResponseEntity<?> getTimeKeepingByFilter(TimeKeepingFilterFrom from){
        long startTime = System.currentTimeMillis();
        LogUtil.showLog(logger,LogUtil.LOG_BEGIN,"getTimeKeepingByFilter",startTime);
        MessagesResponse<List<TimeKeepingDTO>> messagesResponse = new MessagesResponse<>();
        List<TimeKeepingDTO> list = new ArrayList<>();

        try {
            list = modelMapper.map(timeKeepingService.getAllTimeKeepingByFilter(from),new TypeToken<List<TimeKeepingDTO>>(){}.getType());
            messagesResponse.setData(list);
            messagesResponse.setMessage(CoreConstants.MESSAGE.SUCCESS);
        }catch (Exception e){
            logger.error(e.getMessage(),e);
            messagesResponse.error(e);
        }finally {
            LogUtil.showLog(logger,LogUtil.LOG_END,"getTimeKeepingByFilter",startTime);
        }

        return new ResponseEntity<>(messagesResponse,HttpStatus.OK);
    }

    @PostMapping("/updateTimeKeeping")
    @PreAuthorize("@appAuthorizer.authorize(authentication,'" + CoreConstants.PRIVILEGE.UPDATE + "',this)")
    public ResponseEntity<?> updateTimeKeeping(@RequestParam(name = "id") Integer id, @RequestBody TimeKeepingUpdateForm form){
        long startTime = System.currentTimeMillis();
        LogUtil.showLog(logger,LogUtil.LOG_BEGIN,"updateTimeKeeping",startTime);
        MessagesResponse<Object>messagesResponse = new MessagesResponse<>();

        try{
            timeKeepingService.updateTimeKeeping(id, form);
            messagesResponse.setData(form);
            messagesResponse.setMessage(CoreConstants.MESSAGE.UPDATE_SUCCESS);
        }catch (Exception e){
            logger.error(e.getMessage(),e);
            messagesResponse.error(e);
        }finally {
            LogUtil.showLog(logger,LogUtil.LOG_END,"updateTimeKeeping",startTime);
        }

        return new ResponseEntity<>(messagesResponse,HttpStatus.OK);
    }

    @PostMapping("/deleteById")
    @PreAuthorize("@appAuthorizer.authorize(authentication,'" + CoreConstants.PRIVILEGE.DELETE + "',this)")
    public ResponseEntity<?> deleteTimeKeepingById(@RequestParam(name = "id") Integer id){
        long startTime = System.currentTimeMillis();
        LogUtil.showLog(logger,LogUtil.LOG_BEGIN,"deleteTimeKeepingById",startTime);
        MessagesResponse<TimeKeepingDTO> messagesResponse = new MessagesResponse<>();

        try {
            TimeKeepingDTO timeKeepingDTO = modelMapper.map(timeKeepingService.getTimeKeepingById(id),TimeKeepingDTO.class);
            messagesResponse.setData(timeKeepingDTO);
            timeKeepingService.deleteTimeKeepingById(id);
            messagesResponse.setMessage(CoreConstants.MESSAGE.DELETE_SUCCESS);
        }catch (Exception e){
            logger.error(e.getMessage(),e);
            messagesResponse.error(e);
        }finally {
            LogUtil.showLog(logger,LogUtil.LOG_END,"deleteTimeKeepingById",startTime);
        }

        return new ResponseEntity<>(messagesResponse,HttpStatus.OK);
    }


    @PostMapping("/deleteAllTimeKeeping")
    @PreAuthorize("@appAuthorizer.authorize(authentication,'" + CoreConstants.PRIVILEGE.DELETE + "',this)")
    public ResponseEntity<?> deleteAllTimeKeeping(@RequestBody List<Integer> ids){
        long startTime = System.currentTimeMillis();
        LogUtil.showLog(logger,LogUtil.LOG_BEGIN,"deleteAllTimeKeeping",startTime);
        MessagesResponse<List<TimeKeepingDTO>> messagesResponse = new MessagesResponse<>();
        try {
            List<TimeKeeping> timeKeepings = new ArrayList<>();
            for (Integer item : ids){
                TimeKeeping timeKeeping = timeKeepingService.getTimeKeepingById(item);
                timeKeepings.add(timeKeeping);
            }
            messagesResponse.setData(modelMapper.map(timeKeepings,new TypeToken<List<TimeKeepingDTO>>(){}.getType()));
            timeKeepingService.deleteAllTimeKeeping(ids);
            messagesResponse.setMessage(CoreConstants.MESSAGE.DELETE_SUCCESS);
        }catch (Exception e){
            logger.error(e.getMessage(),e);
            messagesResponse.error(e);
        }finally {
            LogUtil.showLog(logger,LogUtil.LOG_END,"deleteAllTimeKeeping",startTime);
        }

        return new ResponseEntity<>(messagesResponse,HttpStatus.OK);
    }
}
