package com.fis.business.specification;

import com.fis.business.entity.Company;
import lombok.Data;
import lombok.NonNull;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Date;

@Data
public class CompanySpecificationCustom implements Specification<Company> {

    @NonNull
    private String filter;

    @NonNull
    private Object value;

    @Override
    public Predicate toPredicate(Root<Company> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {

        if(filter.equalsIgnoreCase("code")){
            return criteriaBuilder.like(root.get("code"),"%" + value.toString() + "%");
        }

        if(filter.equalsIgnoreCase("publicStyle")){
            return criteriaBuilder.equal(root.get("publicStyle"), value);
        }

        if(filter.equalsIgnoreCase("applyDay")){
            return criteriaBuilder.equal(root.get("applyDay").as(java.util.Date.class),(Date) value);
        }

        return null;
    }
}
