package com.fis.business.specification;

import com.fis.business.entity.Company;
import com.fis.business.form.filter.CompanyFilterForm;
import org.springframework.data.jpa.domain.Specification;

public class CompanySpecification {

    public static Specification<Company> buildWhere(CompanyFilterForm form){
        Specification<Company> where = null;

        if (form != null && form.getCode() != null){
            CompanySpecificationCustom filterCode = new CompanySpecificationCustom("code",form.getCode());
            where = Specification.where(filterCode);
        }

        if (form!= null && form.getPublicStyle() != null){
            CompanySpecificationCustom filterStyle = new CompanySpecificationCustom("publicStyle",form.getPublicStyle());

            if (where == null){
                where = filterStyle;
            }else {
                where = where.and(filterStyle);
            }
        }

        if (form!= null && form.getApplyDay() != null){
            CompanySpecificationCustom filterApplyDay = new CompanySpecificationCustom("publicStyle",form.getApplyDay());

            if (where == null){
                where = filterApplyDay;
            }else {
                where = where.and(filterApplyDay);
            }
        }

        return where;
    }
}
