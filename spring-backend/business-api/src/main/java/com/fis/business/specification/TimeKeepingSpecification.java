package com.fis.business.specification;

import com.fis.business.entity.TimeKeeping;
import com.fis.business.form.filter.TimeKeepingFilterFrom;
import org.springframework.data.jpa.domain.Specification;

public class TimeKeepingSpecification {

    public static Specification<TimeKeeping> buildWhere(TimeKeepingFilterFrom from){
        Specification<TimeKeeping> where = null;

        if (from != null && from.getDayInWeek() != null){
            TimeKeepingSpecificationCustom dayInWeekFilter = new TimeKeepingSpecificationCustom("dayInWeek",from.getDayInWeek());
            where = Specification.where(dayInWeekFilter);
        }

        if (from != null && from.getCheckIn() != null){
            TimeKeepingSpecificationCustom checkInFilter = new TimeKeepingSpecificationCustom("checkIn",from.getCheckIn());
            if (where == null) {
                where = checkInFilter;
            }else {
                where = where.and(checkInFilter);
            }
        }

        if (from != null && from.getCheckOut() != null){
            TimeKeepingSpecificationCustom checkOutFilter = new TimeKeepingSpecificationCustom("checkOut",from.getCheckOut());
            if (where == null) {
                where = checkOutFilter;
            }else {
                where = where.and(checkOutFilter);
            }
        }

        return where;
    }
}
