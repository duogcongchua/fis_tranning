package com.fis.business.form.inesrt;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Date;

@Data
public class CompanyCreateForm {

    @NotNull
    @NotBlank(message = "Company name can't null")
    private String name;

    @NotNull
    @NotBlank(message = "Company name can't null")
    private String code;

    @NotNull
    @NotBlank(message = "Company name can't null")
    private String address;

    @NotNull
    @NotBlank(message = "Company name can't null")
    private String nameShift;

    @NotNull
    @NotBlank(message = "Group can't null")
    @Positive(message = "Group > 0")
    private Integer group;

    @NotNull
    @NotBlank(message = "Type ship can't null")
    @Positive(message = "Type ship > 0")
    private Integer typeShift;

    @NotNull
    @NotBlank(message = "Public style can't null")
    @Positive(message = "Public style > 0")
    private Integer publicStyle;

    @NotNull
    @NotBlank(message = "Value shift can't null")
    private String valueShift;

    @NotNull
    @NotBlank(message = "Shift can't null")
    private String shift;

    @NotNull
    @NotBlank(message = "Status can't null")
    private String status;

    @NotNull
    @NotBlank(message = "Apply day can't null")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date applyDay;

    @NotNull
    @NotBlank(message = "Start shift can't null")
    private String startShift;

    @NotNull
    @NotBlank(message = "End shift can't null")
    private String endShift;
}
