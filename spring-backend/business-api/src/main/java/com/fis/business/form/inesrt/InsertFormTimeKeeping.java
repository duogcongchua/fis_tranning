package com.fis.business.form.inesrt;

import lombok.Data;

import java.util.List;

@Data
public class InsertFormTimeKeeping {

    CompanyCreateForm companyCreateForm;

    List<TimeKeepingCreateForm> timeKeepingCreateForms;

}
